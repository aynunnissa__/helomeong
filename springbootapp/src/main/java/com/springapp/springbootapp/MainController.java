package com.springapp.springbootapp;

import com.springapp.springbootapp.model.Penyakit;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {
	
    @GetMapping("/")
    public String index() {
        //Ketika kembali ke homepage, listPenyakit akan dihapus agar user dapat mendiagnosa dari awal lagi
        //tanpa dipengaruhi diagnosa sebelumnya
        Penyakit.deleteListPenyakit();
        return "homepage";
    }

    @GetMapping("/welcome")
    public String welcome(Model model) {
       
        model.addAttribute("penyakit", new Penyakit());
        Penyakit.bukaFile();    //File yang berisi daftar penyakit dan cirinya akan dibuka dan disimpan ke classnya masing2
        return "welcome";
    }

    @PostMapping("/welcome")
    public String welcomeOrgan(@ModelAttribute Penyakit penyakit,
        @RequestParam(value="organ", required=false) String organ) {
            //Switch tergantung dari organ mana yang ditulis cirinya oleh user
            switch(organ) {
                case "Pernapasan" : Penyakit.tambahDiagnosa("Pernapasan", penyakit); break;
                case "Pencernaan" : Penyakit.tambahDiagnosa("Pencernaan", penyakit); break;
                case "Ekskresi" : Penyakit.tambahDiagnosa("Ekskresi", penyakit); break;
                case "SistemIndera" : Penyakit.tambahDiagnosa("SistemIndera", penyakit); break;

            }
        
        return "welcome";
    }

    
    @GetMapping("/hasilDiagnosa")
    public String hasilDiagnosa(Model model) {
        model.addAttribute("hasilDiagnosa", Penyakit.getDaftarPenyakit());
        //Setelah diagnosa selesai dan user akan pindah halaman atau melakukan refresh halaman, list penyakit hasil
        //diagnosa akan hilang
        Penyakit.deleteListPenyakit();
        return "hasilDiagnosa";
    }

}


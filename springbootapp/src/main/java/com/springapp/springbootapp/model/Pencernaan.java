package com.springapp.springbootapp.model;

import java.util.ArrayList;
import java.util.List;

public class Pencernaan extends Penyakit {
	
	//Atribut untuk menyimpan semua list penyakit di organ Pencernaan
	private static List<Penyakit> listSemuaPenyakitPencernaan = new ArrayList<Penyakit>(); 
	
	Pencernaan() {}
	
	Pencernaan(String namaPenyakit, List<String> ciriPenyakit) {
		//Memanggil constructor super class, yaitu Penyakit
		super(namaPenyakit, ciriPenyakit);
	}
	
	//Method untuk menambahkan list penyakit organ pencernaan
	public void addListSemuaPenyakitPencernaan(Penyakit penyakit) {
		//Memastikan tidak ada penyakit dengan nama yang sama muncul lebih dari sekali
		boolean tidakAda = true;
		for (Penyakit Pencernaan : listSemuaPenyakitPencernaan) {
			if (Pencernaan.getNamaPenyakit().equals(penyakit.getNamaPenyakit())) {
				tidakAda = false;
				break;
			}
		}
		if (tidakAda) listSemuaPenyakitPencernaan.add(penyakit);
	}
	
	//Method yang akan mereturn list penyakit di organ pencernaan
	public List<Penyakit> getListSemuaPenyakitPencernaan() {
		return listSemuaPenyakitPencernaan;
	}

}

package com.springapp.springbootapp.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Penyakit {

	//List untuk menyimpan penyakit hasil dari diagnosa
	private static ArrayList<Penyakit> listPenyakitDiagnosa = new ArrayList<Penyakit>(); 
	private String namaPenyakit; //atribut untuk nama penyakit
	private List<String> ciriPenyakit = new ArrayList<String>(); //atribut untuk list ciri penyakit
	
	//Atribut di bawah ini tergantung dari input user, jika user menambah keluhan di organ Pernapasan,
	//maka method ciriPernapasan saja yang akan memiliki value selain default
	private String ciriPernapasan;
	private String ciriPencernaan;
	private String ciriIndera;
	private String ciriEkskresi;

	private int persentase = 0; //atribut persentase kemungkinan penyakit
	private String solusi = "";	//atribut untuk solusi penyakit
	private String persentasePersen = "";	//atribut untuk persentase kemungkinan penyakit dalam persen

	public Penyakit() {}
	
	//Constructor untuk mengisi value dari atribut namaPenyakit dan ciriPenyakit
	Penyakit(String namaPenyakit, List<String> ciriPenyakit) {
		this.namaPenyakit = namaPenyakit;
		this.ciriPenyakit = ciriPenyakit;
	}
	
	//Beberapa setter dan getter untuk semua atribut private

	public void setNamaPenyakit(String nama) {}
	public String getNamaPenyakit() { return this.namaPenyakit;}

	public void setCiriPernapasan(String ciri) {this.ciriPernapasan = ciri;}
	public String getCiriPernapasan() {return this.ciriPernapasan;}

	public void setCiriEkskresi(String ciri) {this.ciriEkskresi = ciri;}
	public String getCiriEkskresi() {return this.ciriEkskresi;}

	public void setCiriPencernaan(String ciri) {this.ciriPencernaan = ciri;}
	public String getCiriPencernaan() {return this.ciriPencernaan;}

	public void setCiriIndera(String ciri) {this.ciriIndera = ciri;}
	public String getCiriIndera() {return this.ciriIndera;}

	public void setPersentase(int persen) {this.persentase = persen;}
	public int getPersentase() {return this.persentase;}

	public void setPersentasePersen(String persentase) {this.persentasePersen = persentase;}
	public String getPersentasePersen() {return this.persentasePersen;}

	public void setSolusi(String solusi) {this.solusi = solusi;}
	public String getSolusi() {return this.solusi;}

	
	//Method untuk membuka semua file daftar penyakit dan cirinya ketika program pertama masuk ke page Diagnosa
	public static void bukaFile() {
		
		mendataPenyakit("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Ekskresi.txt");
		mendataPenyakit("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Pencernaan.txt");
		mendataPenyakit("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Pernapasan.txt");
		mendataPenyakit("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\SistemIndera.txt");
	}

	public static void mendataPenyakit(String namaFile) {
		BufferedReader br;
		try {
			//br akan membaca file berdasarkan namaFile
			br =  new BufferedReader(new FileReader(namaFile)); 

			while(true) {
				String isiBaris = br.readLine();
				//bacaBaris akan terdiri dari nama penyakit di index 0 dan sisanya merupakan ciri penyakit
				String[] bacaBaris = isiBaris.split(","); 
				//If kondisional jika file sudah sampai akhir konten, while loop selesai
				if (bacaBaris[0].equals("END")) break;
				//atribut ciri untuk menyimpan ciri-ciri suatu penyakit
				List<String> ciri = new ArrayList<String>();
				for (String x : bacaBaris) ciri.add(x);
				ciri.remove(0); //Indeks-0 yang berisi nama penyakit akan diremove dari list ciri penyakit
				
				//If kondisional yang akan membuat object berdasarkan file penyakit yang sedang dibaca
				if (namaFile.equals("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Ekskresi.txt")) {
					Ekskresi eksObject = new Ekskresi(bacaBaris[0],ciri); //Instansiasi objek penyakit ekskresi
					if (!eksObject.getListSemuaPenyakitEkskresi().contains(eksObject)) 
					//Menambah object penyakit Ekskresi ke dalam list	
					eksObject.addListSemuaPenyakitEkskresi(eksObject); 
				} else if (namaFile.equals("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Pencernaan.txt")) {
					Pencernaan penObject = new Pencernaan(bacaBaris[0],ciri); //Instansiasi objek penyakit Pencernaan
					if (!penObject.getListSemuaPenyakitPencernaan().contains(penObject)) 
					penObject.addListSemuaPenyakitPencernaan(penObject); 
				} else if (namaFile.equals("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Pernapasan.txt")) {
					Pernapasan perObject = new Pernapasan(bacaBaris[0],ciri); //Instansiasi objek penyakit pernapasan
					if (!perObject.getListSemuaPenyakitPernapasan().contains(perObject)) 
					perObject.addListSemuaPenyakitPernapasan(perObject); 
					
				} else if (namaFile.equals("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\SistemIndera.txt")) {
					SistemIndera siObject = new SistemIndera(bacaBaris[0],ciri); //Instansiasi objek penyakit ekskresi sistem indera
					if (!siObject.getListSemuaPenyakitSistemIndera().contains(siObject)) 
					siObject.addListSemuaPenyakitSistemIndera(siObject); 
					
				}
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	//Method untuk menghapus list penyakit hasil diagnosa ketika user sudah selesai melakukan diagnosa
	public static void deleteListPenyakit() {
		listPenyakitDiagnosa.clear();
	}
	
	public List<Penyakit> getHasilDiagnosa() {
		List<Penyakit> hasilDiagnosa = getDaftarPenyakit();
		System.out.println("Hasil diagnosa menghasilkan penyakit yang mungkin diderita: ");
		return hasilDiagnosa;
	}
	

	//Method untuk menambah diagnosa seiring dengan user menambah inputan keluhan
	public static void tambahDiagnosa(String organDiagnosa,Penyakit penyakit) {
		switch(organDiagnosa) {
		case "Pencernaan" : setListPenyakitDiagnosa("Pencernaan", penyakit.ciriPencernaan);  break;
		case "Pernapasan" : setListPenyakitDiagnosa("Pernapasan", penyakit.ciriPernapasan); break;
		case "SistemIndera" : setListPenyakitDiagnosa("SistemIndera", penyakit.ciriIndera); break;
		case "Ekskresi" : setListPenyakitDiagnosa("Ekskresi", penyakit.ciriEkskresi); break;
		}
	}

	//Method untuk menambah list penyakit hasil diagnosa sesuai nama organ dan cirinya
	public static void setListPenyakitDiagnosa(String namaOrgan, String cirinya) {
		if (namaOrgan.equals("Ekskresi")) {
			Ekskresi eksObject = new Ekskresi();
			//Jika ciri penyakit organ sesuai dengan ciri yang diinput user, maka penyakit itu akan
			//ditambahkan ke listPenyakitDiagnosa
			for (Penyakit x : eksObject.getListSemuaPenyakitEkskresi()) {
				if (x.ciriPenyakit.contains(cirinya)) listPenyakitDiagnosa.add(x);
			}
		} else if (namaOrgan.equals("Pencernaan")) {
			Pencernaan penObject = new Pencernaan();
			//Jika ciri penyakit organ sesuai dengan ciri yang diinput user, maka penyakit itu akan
			//ditambahkan ke listPenyakitDiagnosa
			for (Penyakit x : penObject.getListSemuaPenyakitPencernaan()) {
				if (x.ciriPenyakit.contains(cirinya)) listPenyakitDiagnosa.add(x);
			}
		} else if (namaOrgan.equals("Pernapasan")) {
			Pernapasan perObject = new Pernapasan();
			//Jika ciri penyakit organ sesuai dengan ciri yang diinput user, maka penyakit itu akan
			//ditambahkan ke listPenyakitDiagnosa
			for (Penyakit x : perObject.getListSemuaPenyakitPernapasan()) {
				if (x.ciriPenyakit.contains(cirinya)) {
					listPenyakitDiagnosa.add(x);
				}
			}
		} else if (namaOrgan.equals("SistemIndera")) {
			
			SistemIndera siObject = new SistemIndera();
			//Jika ciri penyakit organ sesuai dengan ciri yang diinput user, maka penyakit itu akan
			//ditambahkan ke listPenyakitDiagnosa
			for (Penyakit x : siObject.getListSemuaPenyakitSistemIndera()) {
				if (x.ciriPenyakit.contains(cirinya)) {
					listPenyakitDiagnosa.add(x);
				}
			}
		}
	}
	
	//Method untuk mencari solusi dari suatu penyakit
	public static String cariSolusi(String namaPenyakit) {
		BufferedReader br;
		String solusiPenyakit = "";
		try {
			br = new BufferedReader(new FileReader("D:\\javaVs\\.vscode\\springbootapp\\src\\main\\resources\\static\\Solusi.txt"));
			while(true) {
				//Memisahkan nama penyakit dan solusinya yang awalnya digabung dengan simbol @
				String[] solusi = br.readLine().split("@");
				//If kondisional ketika sudah sampai konten terahir, while loop berhenti
				if (solusi[0].equals("END")) break;
				//Jika nama penyakit dalam file solusi sama dengan nama penyakit yang dicari,
				//maka value atribut solusiPenyakit akan diubah sesuai dengan solusi dari file tersebut
				if(solusi[0].equals(namaPenyakit)) {
					solusiPenyakit = solusi[1];
					break;
				}
				
			}
			//Kondisi jika solusi tidak ditemukan
			if (solusiPenyakit.equals("")) solusiPenyakit = "Harap segera menghubungi dokter hewan untuk mendapat informasi terkait penyakit ini sekaligus langkah pengobatannya.";
			
		} catch (FileNotFoundException e) {} catch (IOException e) {}
		return solusiPenyakit;
	}

	
	//Method yang akan mengembalikan list penyakit hasil diagnosa yang sudah siap dilihat user
	public static ArrayList<Penyakit> getDaftarPenyakit() {
		ArrayList<Penyakit> daftarPenyakit = new ArrayList<Penyakit>();
		int totalPenyakit = listPenyakitDiagnosa.size(); //atribut totalPenyakit untuk menghitung persentase
		//For loop untuk mengatur persentase penyakit
		for (Penyakit penyakit : listPenyakitDiagnosa) {
			//Jika penyakit belum ada di list daftarPenyakit, maka akan ditambah dan persentasenya menjadi 1,
			//jika sudah ada, persentase akan ditambah
			if (daftarPenyakit.contains(penyakit)) penyakit.persentase++;
			else {
				daftarPenyakit.add(penyakit);
				penyakit.persentase = 1;
			}
		}
		
		//For loop untuk mengatur persentase penyakit dibanding keseluruhan persentase
		for (Penyakit p : daftarPenyakit) {
			p.persentase = p.persentase*100/totalPenyakit;
			p.persentasePersen = p.persentase + "%";
			p.solusi = cariSolusi(p.namaPenyakit); //Method ini juga akan mencari solusi dari penyakit
		}

		//Method yang mengatur agar dafar penyakit yang ditampilkan ke user dimulai dari penyakit dengan
		//persentase tertinggi
		for (int i = 0; i<daftarPenyakit.size(); i++) {
			for (int j =i; j<daftarPenyakit.size(); j++) {
				if (daftarPenyakit.get(i).persentase < daftarPenyakit.get(j).persentase) {
					Penyakit temp = daftarPenyakit.get(i);
					daftarPenyakit.set(i, daftarPenyakit.get(j));
					daftarPenyakit.set(j, temp);
				}
			}
		}

		return daftarPenyakit;
		
	}

	

}

package com.springapp.springbootapp.model;

import java.util.ArrayList;
import java.util.List;

public class Ekskresi extends Penyakit {

	//Atribut untuk menyimpan semua list penyakit di organ Ekskresi
	private static List<Penyakit> listSemuaPenyakitEkskresi = new ArrayList<Penyakit>();
	
	Ekskresi() {}
	
	Ekskresi(String namaPenyakit, List<String> ciriPenyakit) {
		//Memanggil constructor super class, yaitu Penyakit
		super(namaPenyakit, ciriPenyakit);
	}
	
	//Method untuk menambahkan penyakit Ekskresi
	public void addListSemuaPenyakitEkskresi(Penyakit penyakit) {
		boolean tidakAda = true;
		//Memastikan tidak ada penyakit dengan nama yang sama muncul lebih dari sekali
		for (Penyakit Ekskresi : listSemuaPenyakitEkskresi) {
			if (Ekskresi.getNamaPenyakit().equals(penyakit.getNamaPenyakit())) {
				tidakAda = false;
				break;
			}
		}
		if (tidakAda) listSemuaPenyakitEkskresi.add(penyakit);
	}
	
	//Method yang akan mereturn list penyakit di organ pencernaan
	public List<Penyakit> getListSemuaPenyakitEkskresi() {
		return listSemuaPenyakitEkskresi;
	}
}

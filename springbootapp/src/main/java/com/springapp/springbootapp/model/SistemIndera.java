package com.springapp.springbootapp.model;

import java.util.ArrayList;
import java.util.List;

public class SistemIndera extends Penyakit {
	
	//Atribut untuk menyimpan semua list penyakit di organ Sistem Indera
	private static List<Penyakit> listSemuaPenyakitSistemIndera = new ArrayList<Penyakit>(); 
	
	SistemIndera() {}
	
	SistemIndera(String namaPenyakit, List<String> ciriPenyakit) {
		//Memanggil constructor super class, yaitu Penyakit
		super(namaPenyakit, ciriPenyakit);
	}
	
	//Method untuk menambah list penyakit organ sistem indera
	public void addListSemuaPenyakitSistemIndera(Penyakit penyakit) {
		//Memastikan tidak ada penyakit dengan nama yang sama muncul lebih dari sekali
		boolean tidakAda = true;
		for (Penyakit SistemIndera : listSemuaPenyakitSistemIndera) {
			if (SistemIndera.getNamaPenyakit().equals(penyakit.getNamaPenyakit())) {
				tidakAda = false;
				break;
			}
		}
		if (tidakAda) listSemuaPenyakitSistemIndera.add(penyakit);
	}
	
	//Method yang akan mereturn list penyakit di organ pencernaan
	public List<Penyakit> getListSemuaPenyakitSistemIndera() {
		return listSemuaPenyakitSistemIndera;
	}
}

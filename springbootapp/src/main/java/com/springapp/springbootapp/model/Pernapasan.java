package com.springapp.springbootapp.model;

import java.util.ArrayList;
import java.util.List;

public class Pernapasan extends Penyakit {
	
	//Atribut untuk menyimpan semua list penyakit di organ Pernapasan
	private static List<Penyakit> listSemuaPenyakitPernapasan = new ArrayList<Penyakit>(); 
	
	Pernapasan() {}
	
	Pernapasan(String namaPenyakit, List<String> ciriPenyakit) {
		//Memanggil constructor super class, yaitu Penyakit
		super(namaPenyakit, ciriPenyakit);
	}
	
	//Method untuk menambahkan list penyakit organ pernapasan
	public void addListSemuaPenyakitPernapasan(Penyakit penyakit) {
		//Memastikan tidak ada penyakit dengan nama yang sama muncul lebih dari sekali
		boolean tidakAda = true;
		for (Penyakit pernapasan : listSemuaPenyakitPernapasan) {
			if (pernapasan.getNamaPenyakit().equals(penyakit.getNamaPenyakit())) {
				tidakAda = false;
				break;
			}
		}
		if (tidakAda) listSemuaPenyakitPernapasan.add(penyakit);
	}

	//Method yang akan mereturn list penyakit di organ pencernaan
	public List<Penyakit> getListSemuaPenyakitPernapasan() {
		return listSemuaPenyakitPernapasan;
	}
	
}

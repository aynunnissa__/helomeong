# HeloMeong

Project ini dibuat untuk mengerjakan tugas mini project DDP 2, Fasilkom UI 2020.

Note : Sebelumnya kami ingin menyampaikan permintaan maaf atas kendala yang kami alami dalam membaca file. File yang kami butuhkan hanya bisa dibaca dengan melibatkan directory D. Masalah ini akan berdampak pada kemungkinan terjadinya error “File not found” apabila program dijalankan melalui PC yang berbeda, kecuali path diganti dan disesuaikan dengan directory di PC tersebut. Jika Bapak Dosen ataupun tim asdos berkenan mengubah, program yang mengandung pembacaan file tersebut dilakukan di file Penyakit.java yang terletak di dalam folder model. Atas kekurangan tersebut, kami mengucapkan mohon maaf. Terimakasih..